
const poolConnection = require("./connection.js");

let createTableQuery = `CREATE TABLE IF NOT EXISTS users(
    id INTEGER PRIMARY KEY AUTO_INCREMENT,
    playerName TEXT,
    score INTEGER
);`



async function createTable(){
    try{
        let table = await poolConnection.queryExecution(createTableQuery)
    }catch(err){
        console.error(err);
    }
    
}
module.exports = createTable;