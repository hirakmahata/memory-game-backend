const mysql = require('mysql');
const env_variable = require('./config.js');

const connection = mysql.createPool({

    host : env_variable.host,
    database : env_variable.database,
    user : env_variable.user,
    password : env_variable.password

});

// connection.getConnection((err,myConnection)=>{
//     if(err){
//         console.error(err);
//         return;
//     }else{
//         myConnection.release();
//     }
// });

function queryExecution(query,content){
    return new Promise((resolve,reject) => {
        connection.query(query, content,(err,data) => {
            if(err){
                return reject(err);
            }else{
                return resolve(data);
            }
        })
    })
}

module.exports =  { queryExecution };