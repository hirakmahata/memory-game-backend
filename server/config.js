const dotenv = require("dotenv");
const path = require('path');
dotenv.config({path : path.join(__dirname, "../.env")});

module.exports = {
    port : process.env.PORT,
    host : process.env.HOST,
    database : process.env.DATABASE,
    user : process.env.USER,
    password : process.env.PASSWORD
}