const env_variable = require('./config.js');
const CORS = require("cors");
const express = require("express");
const path = require("path");
const router = require("../routes/router.js");
const createTable = require("./createTable.js");

createTable();


const app = express();

app.use(express.json());
app.use(CORS());

app.use(router);


app.use((req,res,next)=>{
    res.status(404).json({
      message : "404 not found."
    }).end();
  });



app.use((err, req, res, next)=>{
    console.error(err.stack);
    res.status(500).json({
      message: "internal server error"
    }).end();
  })
  
    
    
  app.listen(env_variable.port);
  console.log(`server is listening on port: ${env_variable.port}......`);
    