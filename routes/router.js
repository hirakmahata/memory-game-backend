const poolConnection = require("../server/connection.js");
const express = require("express");
const router = express.Router();

router.get("/scoreBoard",(request,response,next) => {
    let query = `SELECT playerName, MIN(score) as lowestMove FROM users GROUP BY playerName ORDER BY lowestMove LIMIT 3;`;
    poolConnection.queryExecution(query)
    .then(data => {
        data = JSON.stringify(data);
        response.writeHead(200,{"Content-Type":"application/json"})
        response.write(data);
        response.end();
    })
    .catch(err => {
        next(err);
    })
});


router.post("/insertScores", (request,response,next) => {


    let playerName = request.body.playerName;
    let score = request.body.score;

    let query = `INSERT INTO users(playerName, score) VALUES ?;`;
    poolConnection.queryExecution(query,[`${playerName}`,score])
    .then(() => {
         response.end();
    })
    .catch(err => {
        next(err);
    })
})


// router.use((err, req, res, next)=>{
//     console.error(err.stack);
//     res.status(500).json({
//       message: "internal server error"
//     }).end();
//   })


  module.exports = router;